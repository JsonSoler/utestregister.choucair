package co.com.utest.project.stepdefinitions;

import co.com.utest.project.model.utestData;
import co.com.utest.project.tasks.Register;
import co.com.utest.project.tasks.openUp;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class utestStepDefinitions {

    @Before
    public void setStage() {
        OnStage.setTheStage(new OnlineCast());
    }


    @Given("^that Andres wants to learn to register in UTEST$")
    public void thatAndresWantsToLearnToRegisterInUTEST() {

    }

    @When("^Register on page Utest$")
    public void registerOnPageUtest(List<utestData> uTestDataList) {
        OnStage.theActorCalled("Andres").wasAbleTo(openUp.thePage(),
                Register.onThePage(uTestDataList.get(0).getFirstName(), uTestDataList.get(0).getLastName(),
                        uTestDataList.get(0).getEmail(), uTestDataList.get(0).getMonth(),
                        uTestDataList.get(0).getDay(), uTestDataList.get(0).getYear(),
                        uTestDataList.get(0).getPostalCode(),uTestDataList.get(0).getComputer(),
                        uTestDataList.get(0).getVersion(), uTestDataList.get(0).getLanguagePc(),
                        uTestDataList.get(0).getPassword(), uTestDataList.get(0).getConfirmPassword()));
    }

    @Then("^Successfully Registered log in$")
    public void SuccessfullyRegisteredlogin() {
        //Ingresar al sistema después de registrarse.

    }
}
