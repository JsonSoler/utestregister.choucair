package co.com.utest.project.tasks;

import co.com.utest.project.userinterface.utestRegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Register implements Task {

    private String firstName;
    private String lastName;
    private String email;
    private String month;
    private String day;
    private String year;
    private String postalCode;
    private String computer;
    private String version;
    private String languagePc;
    private String password;
    private String confirmPassword;

    public Register(String firstName, String lastName, String email, String month, String day,
                    String year, String postalCode,String computer, String version, String languagePc,
                    String password, String confirmPassword) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.month = month;
        this.day = day;
        this.year = year;
        this.postalCode = postalCode;
        this.computer = computer;
        this.version = version;
        this.languagePc = languagePc;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public static Register onThePage(String firstName, String lastName, String email, String month, String day,
                                     String year, String postalCode, String computer, String version, String languagePc,
                                     String password, String confirmPassword) {
        return Tasks.instrumented(Register.class, firstName, lastName, email, month, day, year, postalCode,
                computer, version, languagePc, password, confirmPassword);
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(utestRegisterPage.BUTTON_NEW),
                Enter.theValue(firstName).into(utestRegisterPage.INPUT_FIRSTNAME),
                Enter.theValue(lastName).into(utestRegisterPage.INPUT_LASTNAME),
                Enter.theValue(email).into(utestRegisterPage.INPUT_EMAIL),
                SelectFromOptions.byVisibleText(month).from(utestRegisterPage.INPUT_MONTH),
                SelectFromOptions.byVisibleText(day).from(utestRegisterPage.INPUT_DAY),
                SelectFromOptions.byVisibleText(year).from(utestRegisterPage.INPUT_YEAR),
                Click.on(utestRegisterPage.BUTTON_NEXT),
                Enter.theValue(postalCode).into(utestRegisterPage.INPUT_POSTAL),
                Click.on(utestRegisterPage.BUTTON_NEXT2),
                Click.on(utestRegisterPage.INPUT_COMPUTER),
                Click.on(utestRegisterPage.LIST_COMPUTER.of(computer)),
                Click.on(utestRegisterPage.INPUT_VERSION),
                Click.on(utestRegisterPage.LIST_VERSION.of(version)),
                Click.on(utestRegisterPage.INPUT_LANGUAGEPC),
                Click.on(utestRegisterPage.LIST_LANGUAGEPC.of(languagePc)),
                Click.on(utestRegisterPage.BUTTON_NEXT3),
                Enter.theValue(password).into(utestRegisterPage.INPUT_PASSWORD),
                Enter.theValue(confirmPassword).into(utestRegisterPage.INPUT_CONFIRMPASSWORD),
                Click.on(utestRegisterPage.INPUT_TERMS),
                Click.on(utestRegisterPage.INPUT_POLICY));
                //Click.on(utestRegisterPage.BUTTON_SAVE)

    }

}



