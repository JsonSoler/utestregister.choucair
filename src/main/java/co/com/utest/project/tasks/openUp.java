package co.com.utest.project.tasks;

import co.com.utest.project.userinterface.utestPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;
import org.jetbrains.annotations.NotNull;

public class openUp implements Task {

    private utestPage utestpage;

    public static Performable thePage() {
        return Tasks.instrumented(openUp.class);
    }

    @Override
    public <T extends Actor> void performAs(@NotNull T actor) {
        actor.attemptsTo(Open.browserOn(utestpage));
    }
}
