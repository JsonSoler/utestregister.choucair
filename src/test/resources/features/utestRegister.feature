##Jeison Soler
@stories
Feature:  Register in Utest
  I want to register in UTES

  @scenario1
  Scenario:
 Create account on utest website

    Given  that Andres wants to learn to register in UTEST

    When  Register on page Utest

      | firstName     | lastName | email                  | month | day | year  | postalCode  | computer | version   | languagePc | password   | confirmPassword |
      | Jeison Andres | Soler    | jey_andres@hotmail.com | March | 4   | 1994 | 1001001    | Windows  | 10 64-bit | English    | Jsoler*1994 | Jsoler*1994      |

    Then  Successfully Registered log in