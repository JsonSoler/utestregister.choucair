package co.com.utest.project.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class utestRegisterPage {

    //INPUTS
    public static final Target INPUT_FIRSTNAME = Target.the("First name").located(By.id("firstName"));
    public static final Target INPUT_LASTNAME = Target.the("Last name").located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("Email").located(By.id("email"));
    public static final Target INPUT_MONTH = Target.the("Birth month").located(By.id("birthMonth"));
    public static final Target INPUT_DAY = Target.the("Birth day").located(By.id("birthDay"));
    public static final Target INPUT_YEAR = Target.the("Birth Year").located(By.id("birthYear"));
    public static final Target INPUT_POSTAL = Target.the("Postal Code").located(By.id("zip"));
    public static final Target INPUT_PASSWORD = Target.the("Put Password").located(By.id("password"));
    public static final Target INPUT_CONFIRMPASSWORD = Target.the("Put Password again").located(By.id("confirmPassword"));
    public static final Target INPUT_TERMS = Target.the("Accept Terms").located(By.id("termOfUse"));
    public static final Target INPUT_POLICY = Target.the("Accept security policy").located(By.id("privacySetting"));
    public static final Target INPUT_VERSION = Target.the("Select version").located(By.xpath("//*[@id=\"web-device\"]/div[2]/div[2]/div/div[1]"));
    public static final Target INPUT_LANGUAGEPC = Target.the("Select Laguage of OS").located(By.xpath("//*[@id=\"web-device\"]/div[3]/div[2]/div/div[1]"));
    public static final Target INPUT_COMPUTER = Target.the("Select your computer").located(By.xpath("//*[@id=\"web-device\"]/div[1]/div[2]/div/div[1]"));

    //BUTTONS
    public static final Target BUTTON_NEW = Target.the("Button Register").located(By.xpath("//a[@class='unauthenticated-nav-bar__sign-up']"));
    public static final Target BUTTON_NEXT = Target.the("Next Button").located(By.xpath("//a[@class='btn btn-blue']"));
    public static final Target BUTTON_NEXT2 = Target.the("Next Button").located(By.xpath("//a[@class='btn btn-blue pull-right']"));
    public static final Target BUTTON_NEXT3 = Target.the("Next Button").located(By.xpath("//a[@class='btn btn-blue pull-right']"));
    public static final Target BUTTON_SAVE = Target.the("Save Changes for register").located(By.xpath("//a[@class='btn btn-blue']"));

    //LIST
    public static final Target LIST_COMPUTER = Target.the("Select Computer list").locatedBy("//div[contains(text(),'{0}')]");
    public static final Target LIST_VERSION = Target.the("Select models list").locatedBy("//div[contains(text(),'{0}')]");
    public static final Target LIST_LANGUAGEPC = Target.the("Select Language List").locatedBy("//div[contains(text(),'{0}')]");

}
